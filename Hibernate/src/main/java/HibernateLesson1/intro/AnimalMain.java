package HibernateLesson1.intro;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class AnimalMain {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        //загружаем настройки в Hibernate
        configuration.configure();
        //Создаём "фабрику", которая будет отдавать нам сессии. Сессия - это физическое подключение приложения к бд
        try(SessionFactory sessionFactory = configuration.buildSessionFactory()) {
            //открываем сессию
            Session session = sessionFactory.openSession();
            Animal animal = Animal.builder()
                    .name("Snesjok")
                    .color("White")
                    .type("Cat")
                    .build();
            session.save(animal);
            session.close();
            System.out.println("Success!");

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
