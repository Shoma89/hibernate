package HibernateLesson1.intro.OneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;



public class OneToOneMain {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        try(Session session = sessionFactory.openSession()) {
            Passport passport = new Passport();
            passport.setData("9005 373300");
            session.save(passport);
            Person person = Person.builder()
                    .name("Vladimir")
                    .lastName("Putin")
                    .passport(passport)
                    .build();
            session.save(person);
        }
    }
}
