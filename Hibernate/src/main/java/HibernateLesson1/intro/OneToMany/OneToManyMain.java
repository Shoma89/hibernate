package HibernateLesson1.intro.OneToMany;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class OneToManyMain {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure();
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        try(Session session = sessionFactory.openSession()) {
            Airplane airplane = Airplane.builder().name("AirBase 425").build();
            Passenger passenger1 = Passenger.builder()
                    .firstName("Vladimir")
                    .lastName("Putin")
                    .airplane(airplane)
                    .build();
            Passenger passenger2 = Passenger.builder()
                    .firstName("Sergey")
                    .lastName("Lavrov")
                    .airplane(airplane)
                    .build();
            Passenger passenger3 = Passenger.builder()
                    .firstName("Dmitriy")
                    .lastName("Medvedev")
                    .airplane(airplane)
                    .build();
            session.save(airplane);
            session.save(passenger1);
            session.save(passenger2);
            session.save(passenger3);
        }
    }
}
