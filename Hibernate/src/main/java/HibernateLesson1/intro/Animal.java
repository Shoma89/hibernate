package HibernateLesson1.intro;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
//У JPA есть требования к классам-моделям, которые мы хотим сохранять в бд:
//1. Класс обязательно должен иметь аннотацию @Entity
//2. У класса должно быть как минимум одно поле(может быть несколько), которое будет отвечать за уникальность объектов данного класса(ключ/id)
// А также должна присутствовать аннотация @Id
//3. У класса должен быть дефолтный(по умолчанию/пустой) конструктор. Нужно ставить аннотацию @NoArgsConstructor
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //теперь id будут генерироваться автоматически с помощью БД
    private long id;
    private String name;
    private String color;
    private String type;
}
