package HibernateLesson2;

import HibernateLesson2.Model.CarEntity;
import HibernateLesson2.Model.PersonEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.cfg.Configuration;

public class MainHibernate {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernateLesson2.cfg.xml")
                .addAnnotatedClass(PersonEntity.class)
                .addAnnotatedClass(CarEntity.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        CarEntity car1 = CarEntity.builder().brand("Mercedes-Benz").model("E220").color("Black").build();
        CarEntity car2 = CarEntity.builder().brand("BMW").model("X6").color("White").build();
        CarEntity car3 = CarEntity.builder().brand("Porsche").model("Cayenne").color("Red").build();
        CarEntity car4 = CarEntity.builder().brand("Bentley").model("Continental").color("Blue").build();
        CarEntity car5 = CarEntity.builder().brand("Lada").model("Priora").color("Silver").build();



        //добавление в бд (create) persist
        em.getTransaction().begin();
        car1 = em.find(CarEntity.class, 1L);
        car2 = em.find(CarEntity.class, 2L);
        car3 = em.find(CarEntity.class, 3L);
        car4 = em.find(CarEntity.class, 4L);
        car5 = em.find(CarEntity.class, 5L);
        PersonEntity person1 = PersonEntity.builder().name("Vladimir Putin").age(70).carId(car1).build();
        PersonEntity person2 = PersonEntity.builder().name("Ilon Mask").age(50).carId(car2).build();
        PersonEntity person3 = PersonEntity.builder().name("Sergey Lovrov").age(75).carId(car3).build();
        PersonEntity person4 = PersonEntity.builder().name("Barac Obama").age(55).carId(car4).build();
        PersonEntity person5 = PersonEntity.builder().name("Bill Gates").age(72).carId(car5).build();
        em.persist(person1);
        em.persist(person2);
        em.persist(person3);
        em.persist(person4);
        em.persist(person5);
        em.getTransaction().commit();

        //поиск в бд (select) find
        em.getTransaction().begin(); //начало транзакции
        PersonEntity person = em.find(PersonEntity.class, 12L); //указываем, что мы ищем в бд
        em.getTransaction().commit();//конец транзакции

        System.out.println(person);

        //изменение объекта в бд (update) merge
        em.getTransaction().begin();
        PersonEntity person6 = em.find(PersonEntity.class, 11L);
        person.setName("Ivan");
        person.setAge(18);
        em.merge(person6);
        em.getTransaction().commit();


        //удаление объекта из бд (delete) remove
        em.getTransaction().begin();
        PersonEntity person7 = em.find(PersonEntity.class, 11L);
        em.remove(person7);
        em.getTransaction().commit();
    }
}
