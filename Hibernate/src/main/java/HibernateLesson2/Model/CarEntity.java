package HibernateLesson2.Model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NamedQueries({
        @NamedQuery(name = "CarEntity.findAll", query = "select c from CarEntity c"), //создали запрос всех сущностей таблицы
        @NamedQuery(name = "CarEntity.findByName", query = "select c from CarEntity c where c.brand = :brand")
})

public class CarEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String brand;
    private String model;
    private String color;
}
