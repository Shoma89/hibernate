package HibernateLesson2;

import HibernateLesson2.Model.CarEntity;
import HibernateLesson2.Model.PersonEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class MainJPQL {
    public static void main(String[] args) {
        EntityManagerFactory factory = new Configuration()
                .configure("hibernateLesson2.cfg.xml")
                .addAnnotatedClass(PersonEntity.class)
                .addAnnotatedClass(CarEntity.class)
                .buildSessionFactory();
        EntityManager em = factory.createEntityManager();
        //создаём запрос с помощью JPQL и копируем всё в лист
        List<PersonEntity> personEntityList = em.createQuery("select p from PersonEntity p", PersonEntity.class).getResultList();

        for(PersonEntity temp : personEntityList){
            System.out.println(temp);
        }
        System.out.println("---------------------------------------------------------------------");
        CarEntity car = em.createQuery("select c from CarEntity c where c.model = 'X6'", CarEntity.class).getSingleResult();
        System.out.println(car);
        System.out.println("---------------------------------------------------------------------");

        //создаём запрос с помощью метода createNamedQuery и аннотации @NamedQuery и получаем лист
        List<CarEntity> carEntityList = em.createNamedQuery("CarEntity.findAll", CarEntity.class).getResultList();
        for(CarEntity temp : carEntityList){
            System.out.println(temp);
        }
        System.out.println("-----------------------------------------------------------------------");
        List<CarEntity> carEntityList1 = em.createNamedQuery("CarEntity.findByName", CarEntity.class).
                setParameter("brand", "Lada").getResultList();
        System.out.println(carEntityList1);
    }
}
